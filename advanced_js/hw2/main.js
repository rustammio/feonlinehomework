const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70,
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70,
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70,
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40,
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    },
];
class MissingAuthorError extends Error {
    constructor() {
        super();
        this.name = "MissingAuthorError";
        this.message = `User didn\'t enter the author name!!!!!!`;
    }
}
class MissingNameError extends Error {
    constructor() {
        super();
        this.name = "MissingNameError";
        this.message = `User didn\'t enter the name of the book!!!!!!`;
    }
}
class MissingPriceError extends Error {
    constructor() {
        super();
        this.name = "MissingPriceError";
        this.message = `User didn\'t enter the price!!!!!!`;
    }
}
class CompleteCard {
    constructor(author, name, price) {
        if (author === undefined) {
            throw new MissingAuthorError();
        }
        if (name === undefined) {
            throw new MissingNameError();
        }
        if (price === undefined) {
            throw new MissingPriceError();
        }
        this.author = author;
        this.name = name;
        this.price = price;
        this.root = document.querySelector(".root");
        this.ul = document.createElement("ul");
    }
    render() {
        this.root.append(this.ul);
        this.li = document.createElement("li");
        this.li.innerText = `Author: ${this.author}; Book: ${this.name}; Price: ${this.price}`;
        this.ul.append(this.li);
    }
}

books.forEach((e) => {
    try {
        new CompleteCard(e.author, e.name, e.price).render();
    } catch (e) {
        console.warn(e);
    }
});
