class StarWars {
    constructor({characters, name, episodeId, openingCrawl}) {
        this.root = document.querySelector('.root');
        this.container = document.createElement('div');
        this.episodeNum = document.createElement('p');
        this.episodeName = document.createElement('h2');
        this.episodeCrawl = document.createElement('p');
        this.castContainer = document.createElement('ul');
        this.load = document.createElement('div');
        this.characters = characters;
        this.name = name;
        this.episodeId = episodeId;
        this.openingCrawl = openingCrawl
    }

    #getCharacters() {
        const actors = this.characters.map(el => fetch(el).then(response => response.json()));
        Promise.all(actors)
            .then(response => {
                this.load.innerHTML = '';
                const names = response.map(({name})=> {
                    return `<li>${name}</li>`
                });
                this.load.innerHTML = names.join('\n')
                // response.forEach(({name}) => {
                //     const castMember = document.createElement('li')
                //     castMember.innerText = name
                //     this.castContainer.append(castMember)
                // })
            })
    }

    render() {
        this.episodeName.innerText = this.name;
        this.episodeNum.innerText = this.episodeId;
        this.episodeCrawl.innerText = this.openingCrawl;
        this.root.append(this.container);
        this.container.append(this.episodeName, this.episodeNum, this.episodeCrawl, this.castContainer, this.load);
        this.load.insertAdjacentHTML('beforeend', `<div class="lds-spinner">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            </div>`);
        this.#getCharacters()
    }
}

fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(result => {
        result.forEach((e) => {
            console.log(e);
            return new StarWars(e).render()
        })
    }).catch(e => console.warn(e));
