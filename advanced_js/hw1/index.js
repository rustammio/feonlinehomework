class Employee {
    constructor(name,age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() { return this._name; };
    get age() { return this._age; };
    get salary() { return this._salary; };
    set salary(value) { this._salary = value; };
    set age (value) { this._age = value; };
    set name (value) { this._name = value}
}
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    get salary() { return this._salary * 3; };
}
const sam = new Programmer('Sam', 90, 4000, 'pascal');
const dave = new Programmer('Dave', 23, 3500, 'js');
const mike = new Programmer('Mike', 33, 450, 'go');
console.group();
console.log(sam);
console.log(dave);
console.log(mike);
console.groupEnd();