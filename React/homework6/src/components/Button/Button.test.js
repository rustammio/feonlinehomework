import Button from "./Button";
import {render, screen} from '@testing-library/react'

const facke =
    {
        text: 'hello',
        name: '',
        toggle: jest.fn(),
        backgrounfColor: 'while'
    };
describe('Button', () => {
    test('Will Button render', () => {
        const {asFragment} = render(<Button/>);
        expect(asFragment()).toMatchSnapshot()
    });
    test('text renders properly', () => {
        render(<Button text={'hello'}/>);
        const but = screen.getByRole('button',{
            name:'hello'
        });
        expect(but).toBeInTheDocument()
    });
    test('hash to call the function', ()=>{
        render(<Button toggle={facke.toggle}/>);
        const but= screen.getByRole('button');
        but.click();
        expect(facke.toggle).toBeCalled()
    });
});