import {render} from "@testing-library/react";
import Card from "./Card";

const fackeCard = {
    location: '',
    id:8,
    isFavorite: false,
    rating:{
        avarage: 0,
        reviws: 0
    }
}

describe('Card',()=>{
    test('Card renders',()=>{
        const {asFragment} = render(<Card card={fackeCard}/>);
        expect(asFragment()).toMatchSnapshot()
    })

});