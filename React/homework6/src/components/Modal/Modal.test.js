import Modal from './Modal'
import {render, screen} from '@testing-library/react'


const fackeModal={
    toCrt: jest.fn(),
    toggle: jest.fn(),
    question: 'this it the test question',
    wine: 'wine'
};


describe('Modal', ()=>{
    test('hash to render',()=>{
      const {asFragment}=  render(<Modal toCart={fackeModal.toCrt} toggle={fackeModal.toggle}/>)
        expect(asFragment()).toMatchSnapshot()
    });
    test('Question has to render', ()=>{
        render(<Modal toCart={fackeModal.toCrt} toggle={fackeModal.toggle} question={fackeModal.question}
        />);
        const modalQuestion= screen.getByText(fackeModal.question)
        expect(modalQuestion).toBeInTheDocument()
    });
    test('wine has to render', ()=>{
        render(<Modal toCart={fackeModal.toCrt} wineToAdd={fackeModal.wine} toggle={fackeModal.toggle}
        />);
        const modalWine= screen.getByText(fackeModal.wine)
        expect(modalWine).toBeInTheDocument()
    });

});