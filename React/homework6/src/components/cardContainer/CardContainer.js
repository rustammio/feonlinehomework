import style from './CardContainer.module.scss'
import PropTypes from "prop-types";
import {useContext} from "react";
import {GridContext} from "../../pages/main/Main";

const CardContainer = ({children}) => {
    const grid = useContext(GridContext);
    console.log(grid);
    const classN = grid || typeof grid === 'undefined' ? style.cardContainer : style.cardContainerTable;
    console.log(grid)
    return (

        <div className={classN}>
            {children}
        </div>
    )
};
CardContainer.propTypes = {
    children: PropTypes.node,


};


export default CardContainer