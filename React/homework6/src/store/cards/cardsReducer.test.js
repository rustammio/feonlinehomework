import {
    FETCH_CARDS_SUCCESS,
    FETCH_CARDS_FAIL,
    FETCH_CARDS_REQUEST,
    ADD_TO_CART,
    REMOVE_FROM_CARD,
    ADD_TO_FAV, REMOVE_FROM_FAV
} from './types'
import {cardsReducer} from "./cardsReducer";
import {fetchCards} from "./actions";

describe('cardsReducer',()=>{
    const fetchedData = fetchCards();
    const initialState ={
        loading: false,
        data:[],
        error: ''
    };
    const loadingStatarted ={
        loading: true,
        data:[],
        error: ''
    }; const loadingFailed ={
        loading: false,
        data:[],
        error: 'Loading Fail'
    };
    const loadingDone ={
        loading: false,
        data: fetchCards(),
        error: ''
    };

   test('hash to return initial state',()=>{
       expect(cardsReducer(initialState,{type: ''})).toEqual(initialState)
   });
   test('starts loading data',()=>{
       expect(cardsReducer(initialState, {type: FETCH_CARDS_REQUEST})).toEqual(loadingStatarted)
   });
   test('loading error',()=>{
       expect(cardsReducer(initialState,{type: FETCH_CARDS_FAIL, payload: 'Loading Fail'})).toEqual(loadingFailed)
   });
    test('loading success',()=>{
        expect(cardsReducer(initialState,{type: FETCH_CARDS_SUCCESS,payload: fetchedData})).toEqual(loadingDone)
        }
    )
});