import {cartReducer} from "./cartReducer";
import {INCREMENT_BIN_COUNTER, RESET_CART, DECREMENT_BIN_COUNTER} from './types'



describe('cartReducer', ()=>{
    const initialstate = {
        binCounter: 0,
        areInBin: []
    };
    const stateToBeEqualTo = {
        binCounter: 1,
        areInBin: [5]
    };
    const initialStateFroDecrement = {
        binCounter: 2,
        areInBin: [5,6]
    };

    test('hash to return initial state',()=>{
       expect(cartReducer(initialstate,{type:''})).toEqual(initialstate)
    });
    test('reset cart',()=> {
        expect(cartReducer(initialstate,{type: RESET_CART})).toEqual(initialstate)
    });
    test('cart reducer increment count',()=>{
        expect(cartReducer(initialstate,{type:INCREMENT_BIN_COUNTER,payload: 5})).toEqual(stateToBeEqualTo)
    });
    test('cart reducer decrement count',()=>{
        expect(cartReducer(initialStateFroDecrement,{type:DECREMENT_BIN_COUNTER,payload:6})).toEqual(stateToBeEqualTo)
    });
});
