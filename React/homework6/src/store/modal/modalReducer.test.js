import {CLOSE_MODAL, CLOSE_MODAL_REMOVE, OPEN_MODAL, OPEN_MODAL_REMOVE} from "./types";
import {modalReducer} from "./modalReducer";


describe('modalReducer',()=>{
    const initialState ={
      isOpen: false,
      isOpenRemove: false
    };
    const openModal ={
        isOpen: true,
        isOpenRemove: false
    }
   test('hash to return initial state',()=>{
       expect(modalReducer(initialState,{type:''})).toEqual(initialState)
   });
    test('has to open modal',()=>{
        expect(modalReducer(initialState,{type:OPEN_MODAL})).toEqual(openModal)
    });
    test('has to close modal',()=>{
        expect(modalReducer(openModal,{type:CLOSE_MODAL})).toEqual(initialState)
    })
});