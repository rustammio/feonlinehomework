import './App.css';
import Header from "./components/Header/Header";
import {PureComponent} from "react";
import Modal from "./components/Modal/Modal";
import CardContainer from "./components/cardContainer/CardContainer";

class App extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            favCounter: 0,
            binCounter: 0,
            isOpen: false,
            isOpenRemove: false,
            cardToAdd: null,
            areFavorite: [],
            areInBin: []
        }
    }

    componentDidMount() {
        if (localStorage.getItem('favCounter')) {
            this.setState({favCounter: JSON.parse(localStorage.getItem('favCounter'))})
        }

        if (localStorage.getItem('binCounter')) {
            this.setState({binCounter: JSON.parse(localStorage.getItem('binCounter'))})
        }
        const fav = JSON.parse(localStorage.getItem('areFavorite'));
        if (localStorage.getItem('areFavorite')) {
            this.setState({areFavorite: fav})
        }
        const bin = JSON.parse(localStorage.getItem('areInBin'));
        if (localStorage.getItem('areInBin')) {
            this.setState({areInBin: bin})
        }

        fetch('data/data.json').then(res => res.json()).then(r => {

            const favList = JSON.stringify(localStorage.getItem('areFavorite'));
            const binList = JSON.stringify(localStorage.getItem('areInBin'));
            r.forEach(el => {
                if (favList.includes(el.id)) {
                    return el.isFavorite = true
                }
                return el.isFavorite = false
            });
            r.forEach(el => {
                if (binList.includes(el.id)) {
                    return el.isInbin = true
                }
                return el.isInbin = false
            });

            console.log(r);
            return this.setState({data: r})
        })
    }

    addFavorite = (id) => {
        this.setState((prev) => {

            const current = prev.favCounter + 1;
            localStorage.setItem('favCounter', JSON.stringify(current));

            const curentFavList = [...prev.areFavorite, id];

            localStorage.setItem('areFavorite', JSON.stringify(curentFavList));

            // eslint-disable-next-line
            const obj = prev.data.find(e => {
                if (e.id === id) {
                    return e
                }
            });

            const index = prev.data.indexOf(obj);

            const data = [...prev.data];
            data[index].isFavorite = true;


            return {
                favCounter: current, areFavorite: curentFavList, data
            }
        })
    };

    rmFavorite = (id) => {
        this.setState((prev) => {

            const current = prev.favCounter - 1;
            localStorage.setItem('favCounter', JSON.stringify(current));
            const edit = JSON.parse(localStorage.getItem('areFavorite'));

            const index = edit.findIndex((e) => e === id);

            edit.splice(index, 1);

            localStorage.setItem('areFavorite', JSON.stringify(edit));
            // eslint-disable-next-line
            const obj = prev.data.find(e => {
                if (e.id === id) {
                    return e
                }
            });

            const i = prev.data.indexOf(obj);

            const data = [...prev.data];
            data[i].isFavorite = false;

            return {
                favCounter: current,
                areFavorite: edit,
                data
            }
        })
    };

    toggleModal = (id) => {
        this.setState(prev => {
            if (!prev.isOpen) {
                return {isOpen: !prev.isOpen, cardToAdd: id}
            }
            return {isOpen: !prev.isOpen, cardToAdd: null}
        })
        ;

    };
    toggleModalRemove = (id) => {
        this.setState(prev => {
            if (!prev.isOpenRemove) {
                return {isOpenRemove: !prev.isOpenRemove, cardToAdd: id}
            }
            return {isOpenRemove: !prev.isOpenRemove, cardToAdd: null}
        })
        ;

    };

    addToCart = (id) => {
        this.setState((prev) => {
            const current = prev.binCounter + 1;
            localStorage.setItem('binCounter', JSON.stringify(current));
            const curentBinList = [...prev.areInBin, id];

            localStorage.setItem('areInBin', JSON.stringify(curentBinList));
            // eslint-disable-next-line
            const obj = prev.data.find(e => {
                if (e.id === id) {
                    return e
                }
            });

            const index = prev.data.indexOf(obj);

            const data = [...prev.data];
            data[index].isInbin = true;
            return {
                binCounter: current, areInBin: curentBinList, data
            }
        })

    };
    removeFromBin = (id) => {
        this.setState((prev) => {

            const current = prev.binCounter - 1;
            localStorage.setItem('binCounter', JSON.stringify(current));
            const edit = JSON.parse(localStorage.getItem('areInBin'));

            const index = edit.findIndex((e) => e === id);

            edit.splice(index, 1);

            localStorage.setItem('areInBin', JSON.stringify(edit));
            // eslint-disable-next-line
            const obj = prev.data.find(e => {
                if (e.id === id) {
                    return e
                }
            });

            const i = prev.data.indexOf(obj);

            const data = [...prev.data];
            data[i].isInbin = false;

            return {
                binCounter: current,
                areInBin: edit,
                data
            }
        })
    }

    render() {
        const {cardToAdd} = this.state

        return (
            <div className="App">
                <Header binCounter={this.state.binCounter} favCounter={this.state.favCounter}/>
                <CardContainer data={this.state.data} rmFavorite={this.rmFavorite} addFavorite={this.addFavorite}
                               toggleModal={this.toggleModal} toggleModalRemove={this.toggleModalRemove}/>
                {this.state.isOpen &&
                    <Modal cardToAdd={cardToAdd} toCart={this.addToCart} toggle={this.toggleModal}/>}
                {this.state.isOpenRemove &&
                    <Modal cardToAdd={cardToAdd} title='Remove' body={'Are you sure you want to remove item from the bin?'} toCart={this.removeFromBin} toggle={this.toggleModalRemove}/>}

            </div>
        );
    }

}

export default App;
