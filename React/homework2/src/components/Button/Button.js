import React from "react";
import styles from './Button.module.scss'
import PropTypes from 'prop-types'
class Button extends React.PureComponent{

    render(){
        const{id,toggle, text ,backgroundColor}=this.props;
        return(
            <div className={styles.btn}>
                <button style={{
                    backgroundColor: backgroundColor
                }} onClick={()=>toggle(id)} className={styles.btnItem}>{text}</button>
            </div>
        )
    }
}
Button.propTypes= {
    toggle: PropTypes.func,
    text: PropTypes.string,
    backgroundColor: PropTypes.string

};
Button.defaultProps ={
    backgroundColor: 'black'
};
export default Button