import {PureComponent} from "react";
import Card from "../Card/Card";
import Button from "../Button/Button";
import style from './CardContainer.module.scss'
import PropTypes from "prop-types";

class CardContainer extends PureComponent {
    render() {
        const {data, rmFavorite, addFavorite, toggleModal,toggleModalRemove} = this.props;

        return (

            <div className={style.cardContainer}>
                {data.map(el => <Card rmFavorite={rmFavorite} addFavorite={addFavorite}
                                      key={el.id} card={el}>
                    {!el.isInbin ? <Button id={el.id} toggle={toggleModal} text='Buy' backgroundColor='red'/> :
                        <Button id={el.id} toggle={toggleModalRemove} text='isInBin' backgroundColor='green'/>}

                </Card>)}
            </div>
        )
    }
}

CardContainer.propTypes = {
    data: PropTypes.array,
    rmFavorite: PropTypes.func.isRequired,
    addFavorite: PropTypes.func.isRequired,
    toggleModal: PropTypes.func.isRequired
}
export default CardContainer