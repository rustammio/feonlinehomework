import './App.css';
import Header from "./components/Header/Header";
import {useEffect, useState} from "react";
import Modal from "./components/Modal/Modal";
import {Route, Routes} from "react-router-dom";
import Bin from "./pages/bin/Bin";
import Favorites from "./pages/favorites/Favorites";
import Main  from './pages/main/Main'
const AppFunc = () => {

    const [data, setData] = useState([]);
    const [favCounter, setFavCounter] = useState(0);
    const [binCounter, setBinCounter] = useState(0);
    const [isOpen, setIsOpen] = useState(false);
    const [isOpenRemove, setIsOpenRemove] = useState(false);
    const [cardToAdd, setCardToAdd] = useState(null);
    const [areFavorite, setAreFavorite] = useState([]);
    const [areInBin, setAreInBin] = useState([]);
    useEffect(() => {
        if (localStorage.getItem('favCounter')) {
            setFavCounter(JSON.parse(localStorage.getItem('favCounter')))
        }
        if (localStorage.getItem('binCounter')) {
            setBinCounter(JSON.parse(localStorage.getItem('binCounter')))
        }
        const fav = JSON.parse(localStorage.getItem('areFavorite'));
        if (localStorage.getItem('areFavorite')) {
            setAreFavorite(fav)
        }
        const bin = JSON.parse(localStorage.getItem('areInBin'));
        if (localStorage.getItem('areInBin')) {
            setAreInBin(bin)
        }
        fetch('data/data.json').then(res => res.json()).then(r => {

            const favList = JSON.stringify(localStorage.getItem('areFavorite'));
            const binList = JSON.stringify(localStorage.getItem('areInBin'));
            r.forEach(el => {
                if (favList.includes(el.id)) {
                    return el.isFavorite = true
                }
                return el.isFavorite = false
            });
            r.forEach(el => {
                if (binList.includes(el.id)) {
                    return el.isInbin = true
                }
                return el.isInbin = false
            });
            return setData(r)
        })
    }, []);
    const toggleModal = (id) => {
        if (isOpen) {
            setIsOpen(prevState => !prevState);
            setCardToAdd(null)
        } else {
            setIsOpen(prevState => !prevState);
            setCardToAdd(id)
        }
    };
    const toggleModalRemove = (id) => {
        if (isOpenRemove) {
            setIsOpenRemove(prevState => !prevState);
            setCardToAdd(null)
        } else {
            setIsOpenRemove(prevState => !prevState);
            setCardToAdd(id)
        }
    };
    const addToCart = (id) => {
        setBinCounter(prevState => {
            const cur = prevState + 1;
            localStorage.setItem('binCounter', JSON.stringify(cur));
            return cur
        });

        setAreInBin(prevState => {
            const cur = [...prevState, id];
            localStorage.setItem('areInBin', JSON.stringify(cur));
            return cur;
        });

        setData(prevState => {
            // eslint-disable-next-line
            const obj = prevState.find(e => {
                if (e.id === id) {
                    return e
                }
            });
            const index = prevState.indexOf(obj);
            prevState[index].isInbin = true;
            return prevState
        })
    };
    const removeFromBin = (id) => {
        setBinCounter(prevState => {
            const cur = prevState - 1;
            localStorage.setItem('binCounter', JSON.stringify(cur));
            return cur
        });
        const edit = JSON.parse(localStorage.getItem('areInBin'));
        const index = edit.findIndex((e) => e === id);
        edit.splice(index, 1);
        localStorage.setItem('areInBin', JSON.stringify(edit));
        setAreInBin(edit);
        setData(prevState => {
            // eslint-disable-next-line
            const obj = prevState.find(e => {
                if (e.id === id) {
                    return e
                }
            });
            const i = prevState.indexOf(obj);
            prevState[i].isInbin = false;
            return prevState;
        })
    };
    const addFavorite = (id) => {
        setFavCounter(prevState => {
            const cur = prevState + 1;
            localStorage.setItem('favCounter', JSON.stringify(cur));
            return cur
        });

        setAreFavorite(prevState => {
            const cur = [...prevState, id];
            localStorage.setItem('areFavorite', JSON.stringify(cur));
            return cur
        });

        setData(prevState => {
            // eslint-disable-next-line
            const obj = prevState.find(e => {
                if (e.id === id) {
                    return e
                }

            });
            const index = prevState.indexOf(obj);
            prevState[index].isFavorite = true;
            return prevState;
        })
    };
    const rmFavorite = (id) => {
        setFavCounter(prevState => {
            const cur = prevState - 1;
            localStorage.setItem('favCounter', JSON.stringify(cur));
            return cur;
        });

        const edit = JSON.parse(localStorage.getItem('areFavorite'));
        const index = edit.findIndex((e) => e === id);
        edit.splice(index, 1);
        localStorage.setItem('areFavorite', JSON.stringify(edit));
        setAreFavorite(edit);
        setData(prevState => {
            // eslint-disable-next-line
            const obj = prevState.find(e => {
                if (e.id === id) {
                    return e
                }
            });
            const i = prevState.indexOf(obj);
            prevState[i].isFavorite = false;
            return prevState
        })
    };


    return (
        <div className="App">
            <Header binCounter={binCounter} favCounter={favCounter}/>

                <Routes>
                    <Route path='/' element={ <Main data={data} rmFavorite={rmFavorite} addFavorite={addFavorite}
                                                              toggleModal={toggleModal} toggleModalRemove={toggleModalRemove}/>}/>
                    <Route path='bin' element={<Bin data={data} areInBin={areInBin} rmFavorite={rmFavorite} addFavorite={addFavorite}
                                                    toggleModal={toggleModal} toggleModalRemove={toggleModalRemove}/>}/>
                    <Route path='favorites' element={<Favorites data={data} areFavorite={areFavorite} rmFavorite={rmFavorite} addFavorite={addFavorite}
                                                                toggleModal={toggleModal} toggleModalRemove={toggleModalRemove}/>}/>
                </Routes>

            {isOpen &&
                <Modal cardToAdd={cardToAdd} toCart={addToCart} toggle={toggleModal}/>}
            {isOpenRemove &&
                <Modal cardToAdd={cardToAdd} title='Remove' body={'Are you sure you want to remove item from the bin?'}
                       toCart={removeFromBin} toggle={toggleModalRemove}/>}

        </div>
    );
};
export default AppFunc