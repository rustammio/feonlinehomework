import './App.css';
import Header from "./components/Header/Header";
import {useEffect, useState} from "react";
import Modal from "./components/Modal/Modal";
import {Route, Routes} from "react-router-dom";
import Bin from "./pages/bin/Bin";
import Favorites from "./pages/favorites/Favorites";
import Main from './pages/main/Main'
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {addToCartAC, addToFavoriteAC, fetchCards, removeFromCartAC, removeFromFav} from "./store/cards/actions";
import {closeModal, closeModalRemove, openModal, openModalRemove} from "./store/modal/actons";

const AppFunc = () => {


    const [favCounter, setFavCounter] = useState(0);
    const [binCounter, setBinCounter] = useState(0);
    // const [isOpen, setIsOpen] = useState(false);
    // const [isOpenRemove, setIsOpenRemove] = useState(false);
    const [cardToAdd, setCardToAdd] = useState(null);
    const [areFavorite, setAreFavorite] = useState([]);
    const [areInBin, setAreInBin] = useState([]);
    const data = useSelector(state => state.cardsReducer.data, shallowEqual)
    const isOpen = useSelector(state => state.modalReducer.isOpen)
    const isOpenRemove = useSelector(state => state.modalReducer.isOpenRemove)
    const dispatch = useDispatch();
    useEffect(() => {
        if (localStorage.getItem('favCounter')) {
            setFavCounter(JSON.parse(localStorage.getItem('favCounter')))
        }
        if (localStorage.getItem('binCounter')) {
            setBinCounter(JSON.parse(localStorage.getItem('binCounter')))
        }
        const fav = JSON.parse(localStorage.getItem('areFavorite'));
        if (localStorage.getItem('areFavorite')) {
            setAreFavorite(fav)
        }
        const bin = JSON.parse(localStorage.getItem('areInBin'));
        if (localStorage.getItem('areInBin')) {
            setAreInBin(bin)
        }
        dispatch(fetchCards())
    }, [dispatch]);

    const toggleModal = (id,name) => {
        if (isOpen) {
            // setIsOpen(prevState => !prevState);
            dispatch(closeModal())
            setCardToAdd(null)
        } else {
            // setIsOpen(prevState => !prevState);
            dispatch(openModal())
            setCardToAdd({id,name})
        }
    };
    const toggleModalRemove = (id,name) => {
        if (isOpenRemove) {
            dispatch(closeModalRemove())
            setCardToAdd(null)
        } else {
            dispatch(openModalRemove())
            setCardToAdd({id, name})
        }
    };
    const addToCart = (id) => {
        setBinCounter(prevState => {
            const cur = prevState + 1;
            localStorage.setItem('binCounter', JSON.stringify(cur));
            return cur
        });

        setAreInBin(prevState => {
            const cur = [...prevState, id];
            localStorage.setItem('areInBin', JSON.stringify(cur));
            return cur;
        });
        dispatch(addToCartAC(id))

    };
    const removeFromBin = (id) => {
        setBinCounter(prevState => {
            const cur = prevState - 1;
            localStorage.setItem('binCounter', JSON.stringify(cur));
            return cur
        });
        const edit = JSON.parse(localStorage.getItem('areInBin'));
        const index = edit.findIndex((e) => e === id);
        edit.splice(index, 1);
        localStorage.setItem('areInBin', JSON.stringify(edit));
        setAreInBin(edit);
        dispatch(removeFromCartAC(id))

    };
    const addFavorite = (id) => {
        setFavCounter(prevState => {
            const cur = prevState + 1;
            localStorage.setItem('favCounter', JSON.stringify(cur));
            return cur
        });

        setAreFavorite(prevState => {
            const cur = [...prevState, id];
            localStorage.setItem('areFavorite', JSON.stringify(cur));
            return cur
        });
        dispatch(addToFavoriteAC(id))
    };
    const rmFavorite = (id) => {
        setFavCounter(prevState => {
            const cur = prevState - 1;
            localStorage.setItem('favCounter', JSON.stringify(cur));
            return cur;
        });

        const edit = JSON.parse(localStorage.getItem('areFavorite'));
        const index = edit.findIndex((e) => e === id);
        edit.splice(index, 1);
        localStorage.setItem('areFavorite', JSON.stringify(edit));
        setAreFavorite(edit);
        dispatch(removeFromFav(id))
        // setData(prevState => {
        //     // eslint-disable-next-line
        //     const obj = prevState.find(e => {
        //         if (e.id === id) {
        //             return e
        //         }
        //     });
        //     const i = prevState.indexOf(obj);
        //     prevState[i].isFavorite = false;
        //     return prevState
        // })
    };


    return (
        <div className="App">
            <Header binCounter={binCounter} favCounter={favCounter}/>

            <Routes>
                <Route path='/' element={<Main data={data} rmFavorite={rmFavorite} addFavorite={addFavorite}
                                               toggleModal={toggleModal} toggleModalRemove={toggleModalRemove}/>}/>
                <Route path='bin'
                       element={<Bin data={data} areInBin={areInBin} rmFavorite={rmFavorite} addFavorite={addFavorite}
                                     toggleModal={toggleModal} toggleModalRemove={toggleModalRemove}/>}/>
                <Route path='favorites'
                       element={<Favorites data={data} areFavorite={areFavorite} rmFavorite={rmFavorite}
                                           addFavorite={addFavorite}
                                           toggleModal={toggleModal} toggleModalRemove={toggleModalRemove}/>}/>
            </Routes>

            {isOpen &&
                <Modal wineToAdd={cardToAdd.name} cardToAdd={cardToAdd.id} toCart={addToCart} toggle={toggleModal}/>}
            {isOpenRemove &&
                <Modal cardToAdd={cardToAdd.id } wineToAdd={cardToAdd.name} title='Remove' body={'Are you sure you want to remove item from the bin?'}
                       toCart={removeFromBin} toggle={toggleModalRemove}/>}

        </div>
    );
};
export default AppFunc