import {combineReducers} from "redux";
import {cardsReducer} from "./cards/cardsReducer";
import {modalReducer} from "./modal/modalReducer";

export const rootReducer = combineReducers({
    cardsReducer,
    modalReducer
})