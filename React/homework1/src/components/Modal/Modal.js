import React from "react";
import styles from "./Modal.module.scss";
class Modal extends React.PureComponent {

    render() {
        const { title, body, question, isClose } = this.props.text;
        const { toggle, actions } = this.props;
        return (
            <>
                <div onClick={toggle} className={styles.modalBackdrop}></div>
                <div className={styles.modal}>
                    <div className={styles.modalTitle}>
                        <h4 className={styles.modalTitleText}>{title}</h4>
                        {isClose && (
                            <h1
                                onClick={toggle}
                                className={styles.modalTitleClose}
                            >
                                x
                            </h1>
                        )}
                    </div>
                    <div className={styles.modalBody}>
                        <div className={styles.modalBodyText}>
                            <h5 className={styles.modalBodyTextMain}>{body}</h5>
                            <p className={styles.modalBodyTextQuestion}>
                                {question}
                            </p>
                        </div>
                        <div className={styles.modalBodyBtn}>{actions}</div>
                    </div>
                </div>
            </>
        );
    }
}
export default Modal;
