import React from "react";
import styles from './Button.module.scss'
class Button extends React.PureComponent{

    render(){
        const{toggle, text ,backgroundColor}=this.props;
        return(
            <div className={styles.btn}>
                <button style={{
                    backgroundColor: backgroundColor
                }} onClick={toggle} className={styles.btnItem}>{text}</button>
            </div>
        )
    }
}
export default Button