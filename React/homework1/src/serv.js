

export const firstModalContent = {
    title: 'Do you want to delete this file?',
    body: "Once you delete this file, it won't be possibe to undo this action.",
    question: 'Are you sure you want to delete it?',
    isClose: true
};
export const secondModalContent = {
    title: "Noway don't go",
    body: "where do you think you're going",
    question: 'Never come back',
    isClose: false
};
