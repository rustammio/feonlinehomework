import React from "react";
import './App.css';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import { firstModalContent, secondModalContent} from "./serv";

class App extends React.Component {
    state={
        isFirstModalShown:false,
        isSecondModalShown: false
    };
    showFirst(){
        this.setState((prev)=>{

            return{isFirstModalShown: !prev.isFirstModalShown}
        })
    }
    showSecond(){
        this.setState((prev)=>{

            return{isSecondModalShown: !prev.isSecondModalShown}
        })
    }
   render(){
       // console.log(buttons2)
       return (
           <div className="App">
               <Button backgroundColor='green' toggle={this.showFirst.bind(this)} text='Open first Modal'/>
               <Button  backgroundColor='red' toggle={this.showSecond.bind(this)} text='Open second Modal'/>
               {this.state.isFirstModalShown &&<Modal toggle={this.showFirst.bind(this)}
                                                      text={firstModalContent}
               actions={
                   <div>
                       <button type={'button'} onClick={this.showFirst.bind(this)} className='item'>OK</button>
                       <button type={'button'} onClick={this.showFirst.bind(this)} className='item'>Close</button>
                   </div>}/>}
               {this.state.isSecondModalShown &&<Modal toggle={this.showSecond.bind(this)}
                                                       text={secondModalContent}
               actions={ <div>
                   <button type={"submit"} onClick={this.showSecond.bind(this)} className='item'>GO</button>
                   <button type={'submit'} onClick={this.showSecond.bind(this)}  className='item'>RUN</button>
               </div>}/>}
           </div>
       );
   }

}

export default App;
